#include "AutoRemovebleObjectsList.hpp"

void AutoRemovebleObjectsListImpl::add(QObject* obj)
{
    connect(obj, &QObject::destroyed, this, &AutoRemovebleObjectsListImpl::remove);
    list.push_back(obj);
}

void AutoRemovebleObjectsListImpl::remove(QObject* obj)
{
    list.removeOne(obj);
}

void AutoRemovebleObjectsListImpl::deleteAll()
{
    for (QObject* obj : list)
        delete obj;
}

QList<QObject*>::iterator AutoRemovebleObjectsListImpl::begin()
{
    return list.begin();
}

QList<QObject*>::iterator AutoRemovebleObjectsListImpl::end()
{
    return list.end();
}