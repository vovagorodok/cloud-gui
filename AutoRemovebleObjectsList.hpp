#pragma once

#include <QObject>
#include <QList>
#include <type_traits>

class AutoRemovebleObjectsListImpl : public QObject
{
    Q_OBJECT

public:
    void add(QObject* obj);
    void remove(QObject* obj);
    void deleteAll();
    QList<QObject*>::iterator begin();
    QList<QObject*>::iterator end();

private:
    QList<QObject*> list;
};

template <typename QObjectT>
class AutoRemovebleObjectsList
{
    static_assert(std::is_base_of<QObject, QObjectT>::value, "type must be QObject");
public:
    inline void add(QObjectT* obj)
    {
        list.add(obj);
    };
    inline void remove(QObjectT* obj)
    {
        list.remove(obj);
    };
    inline void deleteAll()
    {
        list.deleteAll();
    };

    class iterator
    {
    public:
        iterator(QList<QObject*>::iterator inner) :
            inner(inner){}
        inline QObjectT *operator*() const { return qobject_cast<QObjectT*>(*inner); }
        inline iterator &operator++() { ++inner; return *this; }
        inline iterator operator++(int) { return iterator(inner++); }
        inline bool operator==(const iterator &o) const noexcept { return inner == o.inner; }
        inline bool operator!=(const iterator &o) const noexcept { return inner != o.inner; }
    private:
        QList<QObject*>::iterator inner;
    };
    inline iterator begin()
    {
        return iterator(list.begin());
    }
    inline iterator end()
    {
        return iterator(list.end());
    }
private:
    AutoRemovebleObjectsListImpl list;
};

template <typename QObjectT>
class OwnAutoRemovebleObjectsList : public AutoRemovebleObjectsList<QObjectT>
{
public:
    ~OwnAutoRemovebleObjectsList()
    {
        AutoRemovebleObjectsList<QObjectT>::deleteAll();
    }
};