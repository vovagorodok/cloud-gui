#include "Core.hpp"
#include "JsonFile.hpp"
#include <QString>
#include <QFile>
#include <QJsonDocument>
#include <QJsonArray>

namespace
{
const QString CONFIG_FILE = "config.json";
}

Core& Core::instance()
{
    static Core* core = new Core();
    return *core;
}

Core::Core() :
    _remotes{}
{}

Remote& Core::createRemote(const QString& name)
{
    Remote* remote = new Remote(name);
    _remotes.add(remote);
    connect(remote, &QObject::destroyed, [this](){ emit remoteDeleted(); });
    emit remoteCreated(*remote);
    return *remote;
}

Task& Core::createTask(Remote& remote, Task::Config config)
{
    return createTask(remote, new Task(remote, config));
}
Task& Core::createTask(Remote& remote, const QJsonObject& config)
{
    return createTask(remote, new Task(remote, config));
}
Task& Core::createTask(Remote& remote, Task* task)
{
    remote.addTask(task);
    connect(task, &QObject::destroyed, [this](){ emit taskDeleted(); });
    emit taskCreated();
    return *task;
}

Core::Remotes& Core::remotes()
{
    return _remotes;
}

void Core::loadConfig()
{
    const auto config = QFile::exists(CONFIG_FILE) ?
                        loadJson(CONFIG_FILE) :
                        QJsonDocument(QJsonArray());

    for (const QString& name : Engine::getAvailableRemotes())
    {
        Remote& remoteRef = createRemote(name);
        for (const auto remote : config.array())
        {
            const auto remoteObject = remote.toObject();
            if (remoteObject["Name"] == name)
            {
                for (const auto task : remoteObject["Tasks"].toArray())
                {
                    const auto taskObject = task.toObject();
                    createTask(remoteRef, taskObject);
                }
            }
        }
    }
}

void Core::saveConfig()
{
    QJsonArray config;
    for (Remote* remote : _remotes)
    {
        QJsonObject remoteObject;
        remoteObject["Name"] = remote->name();
        QJsonArray tasks;
        for (Task* task : remote->tasks())
        {
            tasks.append(task->saveConfig());
        }
        remoteObject["Tasks"] = tasks;
        config.append(remoteObject);
    }
    saveJson(QJsonDocument(config), CONFIG_FILE);
}
