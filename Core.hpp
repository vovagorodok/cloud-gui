#pragma once

#include "AutoRemovebleObjectsList.hpp"
#include "Task.hpp"
#include "Remote.hpp"
#include <QString>

class Core final : public QObject
{
    Q_OBJECT

public:
    static Core& instance();

    using Remotes = OwnAutoRemovebleObjectsList<Remote>;

    Remote& createRemote(const QString& name);
    Task& createTask(Remote&, Task::Config config);
    Remotes& remotes();

    void loadConfig();
    void saveConfig();

signals:
    void remoteCreated(Remote&);
    void remoteDeleted();
    void taskCreated();
    void taskDeleted();

private:
    Task& createTask(Remote&, const QJsonObject& config);
    Task& createTask(Remote&, Task*);

    Core();
    Remotes _remotes;
};