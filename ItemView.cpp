#include "ItemView.hpp"

ItemView::ItemView(QWidget *parent) :
    QFrame(parent)
{
    ui.setupUi(this);

    ui.lessButton->hide();
    ui.questionLabel->hide();
    ui.noButton->hide();
    ui.yesButton->hide();
    ui.moreWidget->hide();

    connect(ui.moreButton, &QPushButton::clicked, [this]()
    {
        ui.moreButton->hide();
        ui.lessButton->show();
        ui.moreWidget->show();
    });

    connect(ui.lessButton, &QPushButton::clicked, [this]()
    {
        ui.lessButton->hide();
        ui.moreButton->show();
        ui.moreWidget->hide();
    });

    connect(ui.deleteButton, &QPushButton::clicked, [this]()
    {
        ui.deleteButton->hide();
        ui.runButton->hide();
        ui.questionLabel->show();
        ui.noButton->show();
        ui.yesButton->show();
    });

    connect(ui.noButton, &QPushButton::clicked, [this]()
    {
        ui.deleteButton->show();
        ui.runButton->show();
        ui.questionLabel->hide();
        ui.noButton->hide();
        ui.yesButton->hide();
    });
}