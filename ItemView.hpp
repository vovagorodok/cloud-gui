#pragma once

#include "ui_ItemView.h"
#include <QFrame>

class ItemView : public QFrame
{
    Q_OBJECT

public:
    explicit ItemView(QWidget *parent = nullptr);

protected:
    Ui::ItemView ui;
};