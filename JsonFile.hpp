#pragma once

#include <QString>
#include <QJsonDocument>

QJsonDocument loadJson(QString fileName);
void saveJson(const QJsonDocument& document, QString fileName);