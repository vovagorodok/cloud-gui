#include "MainApp.hpp"
#include "SysTrayMenuPositionCalc.hpp"
#include <QAction>
#include <QCoreApplication>
#include <QGuiApplication>
#include <QCloseEvent>
#include <QWindow>
#include <QDebug>

MainApp::MainApp(MainView *mainView,
                 RemoteConfigView *remoteConfigView,
                 TaskConfigView *taskConfigView) :
    mainView(mainView),
    remoteConfigView(remoteConfigView),
    taskConfigView(taskConfigView),
    currentView(mainView)
{
    QCoreApplication::setApplicationName("Cloud GUI");
    // setWindowTitle(QCoreApplication::applicationName());
    
    createTrayIcon();
    setIcon();
    connect(trayIcon, &QSystemTrayIcon::activated, this, &MainApp::iconActivated);
    trayIcon->show();

    setWidowParams(mainView);
    setWidowParams(remoteConfigView);
    setWidowParams(taskConfigView);
    connect(mainView, &MainView::createRemote, this, &MainApp::createRemote);
    connect(mainView, &MainView::createRemote, remoteConfigView, &RemoteConfigView::create);
    connect(mainView, &MainView::createTask, this, &MainApp::createTask);
    connect(mainView, &MainView::createTask, taskConfigView, &TaskConfigView::create);
    connect(remoteConfigView, &RemoteConfigView::canceled, this, &MainApp::createCancel);
    connect(remoteConfigView, &RemoteConfigView::created, this, &MainApp::createDone);
    connect(remoteConfigView, &RemoteConfigView::error, this, &MainApp::showError);
    connect(taskConfigView, &TaskConfigView::canceled, this, &MainApp::createCancel);
    connect(taskConfigView, &TaskConfigView::created, this, &MainApp::createDone);
    connect(taskConfigView, &TaskConfigView::error, this, &MainApp::showError);

    Core::instance().loadConfig();
    connect(&Core::instance(), &Core::remoteDeleted, this, &MainApp::saveConfig);
    connect(&Core::instance(), &Core::taskDeleted, this, &MainApp::saveConfig);
    connect(qApp, &QCoreApplication::aboutToQuit, this, &MainApp::quit);
}

void MainApp::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
    case QSystemTrayIcon::MiddleClick:
        // qDebug() << "Click to icon, visible: " << currentView->isVisible();
        if (currentView->isVisible())
            currentView->hide();
        else
            showCurrentWidget();
        break;
    default:
        ;
    }
}

void MainApp::showCurrentWidget()
{
    currentView->showNormal();
    SysTrayMenuPositionCalc(trayIcon).positionWindow(currentView);
}

void MainApp::showNextWidget(QWidget *next)
{
    QScreen *screen = currentView->windowHandle()->screen();
    QPoint pos = currentView->pos();
    next->showNormal();
    currentView->hide();
    next->windowHandle()->setScreen(screen);
    next->move(pos);
    currentView = next;
}

void MainApp::createRemote()
{
    showNextWidget(remoteConfigView);
}

void MainApp::createTask()
{
    showNextWidget(taskConfigView);
}

void MainApp::createCancel()
{
    showNextWidget(mainView);
}

void MainApp::createDone()
{
    showNextWidget(mainView);
    saveConfig();
}

void MainApp::showError(QString msg)
{
    trayIcon->showMessage("Error", msg);
}

void MainApp::saveConfig()
{
    Core::instance().saveConfig();
}

void MainApp::quit()
{
    saveConfig();
    disconnect(&Core::instance(), &Core::remoteDeleted, this, &MainApp::saveConfig);
    disconnect(&Core::instance(), &Core::taskDeleted, this, &MainApp::saveConfig);
}

void MainApp::createTrayIcon()
{
    QAction *restoreAction = new QAction(tr("&Open"), this);
    connect(restoreAction, &QAction::triggered, this, &MainApp::showCurrentWidget);

    QAction *quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);

    trayIconMenu = new QMenu();
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);
}

void MainApp::setIcon()
{
    QIcon icon = QIcon::fromTheme("cloudstatus", QIcon(":/images/cloud.png"));
    trayIcon->setIcon(icon);
    QGuiApplication::setWindowIcon(icon);
    trayIcon->setToolTip("Click to open menu");
}

void MainApp::setWidowParams(QWidget *window)
{
    window->setWindowFlags(Qt::Popup);
    window->setFixedSize({450, 500});
}