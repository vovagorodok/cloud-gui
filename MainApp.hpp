#pragma once

#include "MainView.hpp"
#include "RemoteConfigView.hpp"
#include "TaskConfigView.hpp"
#include "Remote.hpp"
#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QMenu>

class MainApp : public QObject
{
    Q_OBJECT

public:
    MainApp(MainView *mainView,
            RemoteConfigView *remoteConfigView,
            TaskConfigView *taskConfigView);

private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void showCurrentWidget();
    void showNextWidget(QWidget *next);
    void createRemote();
    void createTask();
    void createCancel();
    void createDone();
    void showError(QString msg);
    void saveConfig();
    void quit();

private:
    void createTrayIcon();
    void setIcon();
    void setWidowParams(QWidget *window);

    QWidget *mainView;
    QWidget *remoteConfigView;
    QWidget *taskConfigView;
    QWidget *currentView;
    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;
};