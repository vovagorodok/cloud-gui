#include "MainView.hpp"
#include "Remote.hpp"
#include "RemoteItemView.hpp"
#include <QVBoxLayout>
#include <QPushButton>
#include <QDebug>

MainView::MainView(QWidget *parent) :
    QWidget(parent)
{
    ui.setupUi(this);

    connect(ui.addRemotePushButton, &QPushButton::clicked,
            this, &MainView::createNewRemote);
    connect(&Core::instance(), &Core::remoteCreated,
            this, &MainView::createRemoteItem);
}

void MainView::createNewRemote()
{
    emit createRemote();
}

void MainView::createNewTask(Remote& remote, Task::Type type)
{
    emit createTask(remote, type);
}

void MainView::createRemoteItem(Remote& remote)
{
    RemoteItemView *item = new RemoteItemView(remote);
    ui.remotesVerticalLayout->addWidget(item);
    connect(item, &RemoteItemView::createTask,
            this, &MainView::createNewTask);
}