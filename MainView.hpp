#pragma once

#include "ui_MainView.h"
#include "Core.hpp"
#include <QWidget>

class MainView : public QWidget
{
    Q_OBJECT

public:
    explicit MainView(QWidget *parent = nullptr);
    ~MainView() = default;

signals:
    void createRemote();
    void createTask(Remote& remote, Task::Type type);

private slots:
    void createNewRemote();
    void createNewTask(Remote& remote, Task::Type type);
    void createRemoteItem(Remote& remote);

private:
    Ui::MainView ui;
};