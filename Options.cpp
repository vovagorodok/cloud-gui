#include "Options.hpp"
#include "RcloneEngine.hpp"
#include <QJsonObject>
#include <QJsonArray>
#include <QCompleter>
#include <QFileSystemModel>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QLabel>
#include <QPushButton>
#include <QFileDialog>
#include <QAbstractItemModel>

Option::Option(const QJsonObject& config) :
    _label(new QLabel(config["Name"].toString().replace("_", " ")))
{}
QLabel* Option::label()
{
    return _label;
}
Option::~Option()
{
    delete _label;
}

RemoteOption::RemoteOption(const QJsonObject& config) :
    Option(config),
    _name(config["Name"].toString()),
    _widget(new QLineEdit)
{
    _widget->setText(config["Default"].toString());
    if (config["IsPassword"].toBool())
        _widget->setEchoMode(QLineEdit::Password);
}
RemoteOption::~RemoteOption()
{
    delete _widget;
}
Option::QJsonObjectPair RemoteOption::value()
{
    return {_name, _widget->text()};
}
QWidget* RemoteOption::widget()
{
    return _widget;
}

RemoteOptionWithExample::RemoteOptionWithExample(const QJsonObject& config) :
    Option(config),
    _name(config["Name"].toString()),
    _widget(new QComboBox)
{
    _widget->setEditable(true);
    _widget->setEditText(config["Default"].toString());
    for (const auto var : config["Examples"].toArray())
        _widget->addItem(var.toObject()["Value"].toString());
}
RemoteOptionWithExample::~RemoteOptionWithExample()
{
    delete _widget;
}
Option::QJsonObjectPair RemoteOptionWithExample::value()
{
    return {_name, _widget->currentText()};
}
QWidget* RemoteOptionWithExample::widget()
{
    return _widget;
}

Option* createRemoteOption(const QJsonObject& config)
{
    Option* opt;
    if (config.contains("Examples"))
        opt = new RemoteOptionWithExample(config);
    else
        opt = new RemoteOption(config);

    QWidget* widget = opt->widget();
    widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    widget->setToolTip(config["Help"].toString());

    return opt;
}

TaskOption::TaskOption(const QJsonObject& config) :
    Option(config),
    _name(config["Name"].toString()),
    _widget(new QLineEdit) // TODO: delete _widget // remoes with lyaout
{
}
TaskOption::~TaskOption()
{
    delete _widget;
}
Option::QJsonObjectPair TaskOption::value()
{
    return {_name, _widget->text()};
}
QWidget* TaskOption::widget()
{
    return _widget;
}

TaskOptionEnum::TaskOptionEnum(const QJsonObject& config) :
    Option(config),
    _name(config["Name"].toString()),
    _widget(new QComboBox)
{
    for (const auto var : config["Values"].toArray())
        _widget->addItem(var.toString());
}
TaskOptionEnum::~TaskOptionEnum()
{
    delete _widget;
}
Option::QJsonObjectPair TaskOptionEnum::value()
{
    return {_name, _widget->currentText()};
}
QWidget* TaskOptionEnum::widget()
{
    return _widget;
}

TaskOptionBool::TaskOptionBool(const QJsonObject& config) :
    Option(config),
    _name(config["Name"].toString()),
    _widget(new QCheckBox)
{
}
TaskOptionBool::~TaskOptionBool()
{
    delete _widget;
}
Option::QJsonObjectPair TaskOptionBool::value()
{
    return {_name, _widget->isChecked()};
}
QWidget* TaskOptionBool::widget()
{
    return _widget;
}

// class FileSystemModel : public QFileSystemModel
// {
// public:
//     FileSystemModel(QObject *parent = 0);
//     QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
// };
// FileSystemModel::FileSystemModel(QObject *parent)
//     : QFileSystemModel(parent)
// {
// }
// QVariant FileSystemModel::data(const QModelIndex &index, int role) const
// {
//     // qDebug() << "Column: " << index.column() << ", Row: " << index.row();
//     if (role == Qt::DisplayRole && index.column() == 0) {
//         QString path  = QDir::toNativeSeparators(filePath(index));
//         if (path.endsWith(QDir::separator()))
//             path.chop(1);
//         return path;
//     }

//     return QFileSystemModel::data(index, role);
// }

// class RcloneFileSystemModel : public QAbstractItemModel
// {
//     // Q_OBJECT
//     struct Node
//     {
        
//     };
// public:
//     RcloneFileSystemModel(RcloneEngine& engine) :
//         _engine(engine)
//     {}
//     QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override
//     {
//         qDebug() << "index: " << "row: " << row << ", column: " << column;
//         auto parentData = parent.data(Qt::DisplayRole).toString();
//         auto out = _engine.listDirectory("Google", parentData);
//         if (out.first != Rclone::Status::Ok)
//             return QModelIndex();
//         return createIndex(row, column, new QString(out.second[row]["Path"].toString()));
//     }
//     QModelIndex parent(const QModelIndex &child) const override
//     {
//         qDebug() << "parent";
//         auto childData = child.data(Qt::DisplayRole).toString();
//         int indexOfSeparator = childData.lastIndexOf("/");
//         if (indexOfSeparator == -1) return QModelIndex(); // No parent

//         return createIndex(0, 0);
//     }
//     int rowCount(const QModelIndex &parent = QModelIndex()) const override
//     {
//         qDebug() << "rowCount";
//         auto parentData = parent.data(Qt::DisplayRole).toString();
//         auto out = _engine.listDirectory("Google", parentData);
//         return out.second.size();
//     }
//     int columnCount(const QModelIndex &parent = QModelIndex()) const override
//     {
//         qDebug() << "columnCount";
//         return 1;
//     }
//     QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override
//     {
//         qDebug() << "data: " << "row: " << index.row() << ", column: " << index.column();
//         if (role != Qt::DisplayRole) return QVariant();
//         return index.data(Qt::DisplayRole).toString();;
//     }
// private:
//     RcloneEngine& _engine;
// };

TaskOptionLocalPath::TaskOptionLocalPath(const QJsonObject& config) :
    Option(config),
    _name(config["Name"].toString()),
    _widget(new QWidget),
    _lineEdit(new QLineEdit)

{
    QCompleter* completer = new QCompleter(this);
    completer->setMaxVisibleItems(10);
    _model = new QFileSystemModel(completer);
    _model->setFilter(QDir::Dirs|QDir::Drives|QDir::NoDotAndDotDot|QDir::AllDirs);
    _model->setRootPath(QDir::homePath());
    completer->setModel(_model);
    // completer->setModel(new MyFileSystemModel);
    completer->setCompletionMode(QCompleter::PopupCompletion);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setWrapAround(true);
    _lineEdit->setCompleter(completer);
    _lineEdit->setText(QDir::homePath());

    QPushButton* button = new QPushButton("Browse");
    connect(button, &QPushButton::clicked, [this]()
    {
        QString dir = QFileDialog::getExistingDirectory(
            _widget,
            tr("Open Directory"),
            _lineEdit->text(),
            QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
        if (not dir.isEmpty())
        {
            _model->setRootPath(dir);
            _lineEdit->setText(dir);
        }
    });

    QHBoxLayout* layout = new QHBoxLayout(_widget);
    layout->addWidget(_lineEdit);
    layout->addWidget(button);
    layout->setMargin(0);
    _widget->setLayout(layout);
}
TaskOptionLocalPath::~TaskOptionLocalPath()
{
    delete _widget;
}
Option::QJsonObjectPair TaskOptionLocalPath::value()
{
    return {_name, _lineEdit->text()};
}
QWidget* TaskOptionLocalPath::widget()
{
    return _widget;
}

Option* createTaskOption(const QJsonObject& config)
{
    Option* opt;
    if (config["Type"].toString() == "Enum")
        opt = new TaskOptionEnum(config);
    else if (config["Type"].toString() == "Bool")
        opt = new TaskOptionBool(config);
    else if (config["Type"].toString() == "LocalPath")
        opt = new TaskOptionLocalPath(config);
    else
        opt = new TaskOption(config);

    QWidget* widget = opt->widget();
    widget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    widget->setToolTip(config["Description"].toString());

    return opt;
}