#pragma once

#include <QObject>

class QJsonObject;
class QLineEdit;
class QComboBox;
class QCheckBox;
class QLabel;
class QFileSystemModel;

class Option : public QObject
{
    Q_OBJECT

public:
    Option(const QJsonObject& config);
    ~Option();

    using QJsonObjectPair = QPair<QString, QJsonValue>;
    virtual QJsonObjectPair value() = 0;
    virtual QWidget* widget() = 0;
    virtual QLabel* label() final;

private:
    QLabel* _label;
};

class RemoteOption : public Option
{
    Q_OBJECT

public:
    RemoteOption(const QJsonObject& config);
    ~RemoteOption();

    QJsonObjectPair value() override;
    QWidget* widget() override;

private:
    const QString _name;
    QLineEdit* _widget;
};

class RemoteOptionWithExample : public Option
{
    Q_OBJECT

public:
    RemoteOptionWithExample(const QJsonObject& config);
    ~RemoteOptionWithExample();

    QJsonObjectPair value() override;
    QWidget* widget() override;

private:
    const QString _name;
    QComboBox* _widget;
};

class TaskOption : public Option
{
    Q_OBJECT

public:
    TaskOption(const QJsonObject& config);
    ~TaskOption();

    QJsonObjectPair value() override;
    QWidget* widget() override;

private:
    const QString _name;
    QLineEdit* _widget;
};

class TaskOptionEnum : public Option
{
    Q_OBJECT

public:
    TaskOptionEnum(const QJsonObject& config);
    ~TaskOptionEnum();

    QJsonObjectPair value() override;
    QWidget* widget() override;

private:
    const QString _name;
    QComboBox* _widget;
};

class TaskOptionBool : public Option
{
    Q_OBJECT

public:
    TaskOptionBool(const QJsonObject& config);
    ~TaskOptionBool();

    QJsonObjectPair value() override;
    QWidget* widget() override;

private:
    const QString _name;
    QCheckBox* _widget;
};

class TaskOptionLocalPath : public Option
{
    Q_OBJECT

public:
    TaskOptionLocalPath(const QJsonObject& config);
    ~TaskOptionLocalPath();

    QJsonObjectPair value() override;
    QWidget* widget() override;

private:
    const QString _name;
    QWidget* _widget;
    QLineEdit* _lineEdit;
    QFileSystemModel* _model;
};

Option* createRemoteOption(const QJsonObject& config);
Option* createTaskOption(const QJsonObject& config);