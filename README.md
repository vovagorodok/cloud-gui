# First steps
```console
sudo apt install cmake qtbase5-dev golang
git clone --recursive  https://gitlab.com/vovagorodok/cloud-gui.git
cd cloud-gui
mkdir build
cd build
cmake ../
make
```
