#include "Rclone.hpp"
#include "librclone.h"
#include <QJsonDocument>
#include <QJsonParseError>
#include <QDebug>

Rclone& Rclone::instance()
{
    static Rclone inst;
    return inst;
}

Rclone::Rclone()
{
    RcloneInitialize();
}

Rclone::~Rclone()
{
    RcloneFinalize();
}

Rclone::Result Rclone::rpc(Rclone::Input input) const
{
    RcloneRPCResult out = RcloneRPC(input.method.toLocal8Bit().data(),
                                    QJsonDocument(input.input).toJson(QJsonDocument::Compact).data());
    QJsonParseError parseError;
    const auto doc = QJsonDocument::fromJson(out.Output, &parseError);

    Status status = Status::Ok;
    if (out.Status != 200)
    {
        status = Status::InternalError;
        qWarning() << "Internal librclone error: " << out.Status;
    }
    if (parseError.error != QJsonParseError::NoError)
    {
        status = Status::JsonParseError;
        qWarning() << "Parse error at" << parseError.offset << ":" << parseError.errorString();
    }
    free(out.Output);

    return {status, doc.object()};
}