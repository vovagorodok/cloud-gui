#pragma once

#include <QString>
#include <QJsonObject>

struct Rclone final
{
    enum class Status
    {
        Ok,
        InternalError,
        JsonParseError
    };

    struct Result
    {
        Status status;
        QJsonObject output;
    };

    struct Input
    {
        QString method;
        QJsonObject input;
    };

    static Rclone& instance();
    Result rpc(Input) const;
private:
    Rclone();
    ~Rclone();
};
