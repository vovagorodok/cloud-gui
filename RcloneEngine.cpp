#include "RcloneEngine.hpp"
#include "JsonFile.hpp"
#include <QFile>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>
#include <QMap>
#include <QDebug>

// RC iterface:
// file://./librclone/rclone/fs/operations/rc.go
// file://./librclone/rclone/fs/config/rc.go
// file://./librclone/rclone/fs/sync/rc.go
// file://./librclone/rclone/cmd/mountlib/rc.go
// And tests are at rc_test.go
// Additional md:
// file://./librclone/rclone/docs/content/rc.md

namespace Engine
{
Rclone::Result getRemoteConfigTemplates()
{
    return Rclone::instance().rpc({"config/providers", QJsonObject()});
}
QVector<QJsonObject> remoteConfigTemplatesAsVector(Rclone::Result output)
{
    QVector<QJsonObject> configs;
    for (const auto config : output.output["providers"].toArray())
        configs.push_back(config.toObject());
    return configs;
}

Rclone::Result createRemote(QJsonObject config)
{
    return Rclone::instance().rpc({"config/create", config});
}

Rclone::Result deleteRemote(QString name)
{
    QJsonObject params;
    params["name"] = name;
    return Rclone::instance().rpc({"config/delete", params});
}

QList<QString> getAvailableRemotes()
{
    QList<QString> res;
    const auto listremotes = Rclone::instance().rpc({"config/listremotes", QJsonObject()});
    for (const auto it : listremotes.output["remotes"].toArray())
        res.push_back(it.toString());
    return res;
}

QJsonObject createDirectoryOperationInput(QString remoteName, QString path)
{
    QJsonObject input{};
    input["fs"] = remoteName + ":";
    input["remote"] = path;
    return input;
}

QList<QJsonObject> convertListDirectoryToList(Rclone::Result output)
{
    QList<QJsonObject> list;
    for (const auto item : output.output["list"].toArray())
        list.push_back(item.toObject());
    return list;
}

Rclone::Result listDirectory(QString remoteName, QString path)
{
    return Rclone::instance().rpc({"operations/list", createDirectoryOperationInput(remoteName, path)});
}
Rclone::Result makeDirectory(QString remoteName, QString path)
{
    return Rclone::instance().rpc({"operations/mkdir", createDirectoryOperationInput(remoteName, path)});
}

QVector<QJsonObject> loadTaskTemplates()
{
    QVector<QJsonObject> tasks;
    static auto doc = loadJson("RcloneTaskTemplates.json");
    for (const auto task : doc.object()["RcloneConfug"].toArray())
        tasks.push_back(task.toObject());
    return tasks;
}

QVector<QJsonObject> getTaskTemplates()
{
    static auto tasks = loadTaskTemplates();
    return tasks;
}

Rclone::Result runTask(QJsonObject task)
{
    static const QMap<QString, QString> commands =
    {
        {"Mount", "mount/mount"},
        {"Unmount", "mount/unmount"},
        {"Sync", "sync/sync"},
        {"Copy", "sync/copy"},
        {"Move", "sync/move"},
        {"Noop", "rc/noop"}
    };
    const auto command = commands.value(task["Name"].toString(), "");
    const auto params = task["Params"].toObject();
    return Rclone::instance().rpc({command, params});
}

} // Engine