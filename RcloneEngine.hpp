#pragma once
#include "Rclone.hpp"
#include <QString>
#include <QList>
#include <QVector>
#include <QJsonObject>

namespace Engine
{
Rclone::Result getRemoteConfigTemplates();
Rclone::Result createRemote(QJsonObject config);
Rclone::Result deleteRemote(QString name);
QList<QString> getAvailableRemotes();
Rclone::Result listDirectory(QString remoteName, QString path);
Rclone::Result makeDirectory(QString remoteName, QString path);
QVector<QJsonObject> getTaskTemplates();
Rclone::Result runTask(QJsonObject task);

QVector<QJsonObject> remoteConfigTemplatesAsVector(Rclone::Result);
QList<QJsonObject> convertListDirectoryToList(Rclone::Result);
}