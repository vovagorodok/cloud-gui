#include "Remote.hpp"

Remote::Remote(const QString& name) :
    _name(name),
    _tasks()
{}

const QString& Remote::name() const
{
    return _name;
}

void Remote::deleteRemote()
{
    Engine::deleteRemote(_name);
    delete this;
}

void Remote::addTask(Task* task)
{
    _tasks.add(task);
    emit taskAdded(*task);
}

Remote::Tasks& Remote::tasks()
{
    return _tasks;
}