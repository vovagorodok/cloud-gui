#pragma once

#include "AutoRemovebleObjectsList.hpp"
#include "Task.hpp"
#include "RcloneEngine.hpp"
#include <QString>
#include <QObject>


class Remote final : public QObject
{
    Q_OBJECT

public:
    Remote(const QString& name);

    using Tasks = OwnAutoRemovebleObjectsList<Task>;

    const QString& name() const;
    void deleteRemote();
    void addTask(Task*);
    Tasks& tasks();

signals:
    void taskAdded(Task&);

private:
    const QString _name;
    Tasks _tasks;
};