#include "RemoteConfigView.hpp"
#include "TaskItemView.hpp"
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include <QFile>
#include <QLineEdit>
#include <QLabel>
#include <QFormLayout>
#include <QComboBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QRegExpValidator>
#include <QDebug>

namespace
{
QString getValidName(QString description)
{
    description.replace(description.indexOf(QRegExp("[^A-Za-z0-9_ -]")),
                        description.size(),
                        "");
    description.replace(description.lastIndexOf(QRegExp("[^\\ ]")) + 1,
                        description.size(),
                        "");
    return description;
}
} // namespace

RemoteConfigView::RemoteConfigView(QWidget *parent) :
    _configs(Engine::remoteConfigTemplatesAsVector(Engine::getRemoteConfigTemplates())),
    _isNameManualyEdited(false),
    _remoteOptions(),
    QWidget(parent)
{
    ui.setupUi(this);

    for (const auto& optionType : {"Simple", "Normal", "Advanced"})
        ui.optionsTypeComboBox->addItem(optionType);

    for (const auto& config : _configs)
        ui.typeComboBox->addItem(config["Description"].toString());

    connect(ui.typeComboBox, qOverload<int>(&QComboBox::currentIndexChanged),
            this, &RemoteConfigView::cloudTypeChanged);
    connect(ui.optionsTypeComboBox, qOverload<int>(&QComboBox::currentIndexChanged),
            this, [this](){ cloudTypeChanged(ui.typeComboBox->currentIndex()); });
    connect(ui.connectPushButton, &QPushButton::clicked,
            this, &RemoteConfigView::createConfig);
    connect(ui.cancelPushButton, &QPushButton::clicked,
            [this](){ emit canceled(); });
    connect(ui.nameLineEdit, &QLineEdit::textChanged,
            this, &RemoteConfigView::validateProvidedConfig);
    connect(ui.nameLineEdit, &QLineEdit::textEdited,
            [this](){ _isNameManualyEdited = true; });

    ui.nameLineEdit->setValidator(new QRegExpValidator(QRegExp("[A-Za-z0-9_ -]*")));
    initConfig();
}

void RemoteConfigView::create()
{
    _isNameManualyEdited = false;
    initConfig();
}

void RemoteConfigView::cloudTypeChanged(int index)
{
    initConfig();
}

void RemoteConfigView::createConfig()
{
    int index = ui.typeComboBox->currentIndex();
    QJsonObject input;
    input["name"] = ui.nameLineEdit->text();
    input["type"] = _configs[index]["Prefix"].toString();

    QJsonObject parameters;
    for (Option* opt : _remoteOptions)
    {
        const auto& value = opt->value();
        parameters[value.first] = value.second;
    }
    input["parameters"] = parameters;

    hide(); // TODO: hot fix for system freeze
    const auto output = Engine::createRemote(input);
    
    qDebug() << "input: " << input;
    // qDebug() << "output: " << output.output;
    if (output.status == Rclone::Status::Ok)
    {
        Core::instance().createRemote(ui.nameLineEdit->text());
        emit created();
    }
    else if (output.status == Rclone::Status::InternalError)
        emit error("Rclone: " + output.output["error"].toString());
    else if (output.status == Rclone::Status::JsonParseError)
        emit error("Error of json parsing");
}

void RemoteConfigView::validateProvidedConfig()
{
    QString name = ui.nameLineEdit->text();

    bool isNameValid = true;
    ui.connectPushButton->setToolTip("Tap to connect");
    if (name.isEmpty())
    {
        isNameValid = false;
        ui.connectPushButton->setToolTip("Name is empty");
    }
    if (Engine::getAvailableRemotes().contains(name))
    {
        isNameValid = false;
        ui.connectPushButton->setToolTip("Name already exist");
    }
    ui.connectPushButton->setEnabled(isNameValid);
}

void RemoteConfigView::initConfig()
{
    const int typeIndex = ui.typeComboBox->currentIndex();
    if (not _isNameManualyEdited)
        ui.nameLineEdit->setText(getValidName(_configs[typeIndex]["Description"].toString()));

    _remoteOptions.deleteAll();

    bool useSimpleOptions = ui.optionsTypeComboBox->currentText() == "Simple";
    bool useNormalOptions = ui.optionsTypeComboBox->currentText() == "Normal";

    for (const auto& option: _configs[typeIndex]["Options"].toArray())
    {
        const auto config = option.toObject();
        bool isRequiredOption = config["Required"].toBool();
        bool isAdvancedOption = config["Advanced"].toBool();
        bool isNormalOption = not isAdvancedOption;
        bool isSimpleOption = not isAdvancedOption and isRequiredOption;
        
        if ((useSimpleOptions and not isSimpleOption) or
            (useNormalOptions and not isNormalOption))
            continue;

        Option* opt = createRemoteOption(config);
        _remoteOptions.add(opt);
        ui.optionsFormLayout->addRow(opt->label(), opt->widget());
    }

    validateProvidedConfig();
}