#pragma once

#include "ui_RemoteConfigView.h"
#include "Core.hpp"
#include "RcloneEngine.hpp"
#include "Remote.hpp"
#include "AutoRemovebleObjectsList.hpp"
#include "Options.hpp"
#include <QWidget>
#include <QVector>
#include <QJsonArray>

class RemoteConfigView : public QWidget
{
    Q_OBJECT

public:
    explicit RemoteConfigView(QWidget *parent = nullptr);

signals:
    void canceled();
    void created();
    void error(QString msg);

public slots:
    void create();

private slots:
    void cloudTypeChanged(int index);
    void createConfig();
    void validateProvidedConfig();

private:
    void initConfig();

    QVector<QJsonObject> _configs;
    bool _isNameManualyEdited;
    OwnAutoRemovebleObjectsList<Option> _remoteOptions;
    Ui::RemoteConfigView ui;
};