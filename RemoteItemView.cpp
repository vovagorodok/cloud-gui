#include "RemoteItemView.hpp"
#include "TaskItemView.hpp"
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include <QFile>
#include <QLineEdit>
#include <QLabel>
#include <QFormLayout>
#include <QComboBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QDebug>

RemoteItemView::RemoteItemView(Remote& remote,
                               QWidget *parent) :
    ItemView(parent),
    _remote(remote)
{
    QHBoxLayout* layout = new QHBoxLayout();
    layout->addWidget(new QLabel(_remote.name(), this));
    // layout->setMargin(0);
    ui.frame->setLayout(layout);

    connect(ui.yesButton, &QPushButton::clicked, this, &RemoteItemView::deleteClicked);
    connect(ui.runButton, &QPushButton::clicked, this, &RemoteItemView::runClicked);

    addTaskButton = new QPushButton(QIcon::fromTheme("list-add"), tr("Add backup"));
    ui.moreButtonsLayout->addWidget(addTaskButton);
    connect(addTaskButton, &QPushButton::clicked, [this]()
    {
        emit createTask(_remote, Task::Type::Sync);
    });  

    connect(&remote, &Remote::taskAdded, [this](Task& task)
    {
        ui.moreInfoLayout->addWidget(new TaskItemView(task));
    });
    for (Task* task : _remote.tasks())
        ui.moreInfoLayout->addWidget(new TaskItemView(*task));
}

void RemoteItemView::showClicked()
{
}

void RemoteItemView::deleteClicked()
{
    _remote.deleteRemote();
    delete this;
}

void RemoteItemView::runClicked()
{
}