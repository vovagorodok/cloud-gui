#pragma once

#include "ItemView.hpp"
#include "Remote.hpp"

class RemoteItemView final : public ItemView
{
    Q_OBJECT

public:
    explicit RemoteItemView(Remote& remote,
                            QWidget *parent = nullptr);

signals:
    void createTask(Remote& remote, Task::Type type);

private slots:
    void showClicked();
    void deleteClicked();
    void runClicked();

private:
    Remote& _remote;
    QPushButton* addTaskButton;
};