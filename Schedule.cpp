#include "Schedule.hpp"

Schedule Schedule::manually()
{
    return {QSharedPointer<EveryMinuteSchedule>::create(QDateTime::currentDateTime(), 0)};
}
Schedule Schedule::immediately()
{
    return {QSharedPointer<EveryMinuteSchedule>::create(QDateTime::currentDateTime(), 0)};
}
Schedule Schedule::once(QDateTime at)
{
    return {QSharedPointer<EveryMinuteSchedule>::create(QDateTime::currentDateTime(), 0)};
}
Schedule Schedule::whenDirChanged(QString path, int waitInMsecs)
{
    return {QSharedPointer<WhenDirChangedSchedule>::create(path, waitInMsecs)};
}
Schedule Schedule::everyMinute(QDateTime startAt, int num)
{
    return {QSharedPointer<EveryMinuteSchedule>::create(startAt, num)};
}
Schedule Schedule::everyHour(QDateTime startAt, int num)
{
    return {QSharedPointer<EveryHourSchedule>::create(startAt, num)};
}
Schedule Schedule::everyDay(QDateTime startAt, int num)
{
    return {QSharedPointer<EveryDaySchedule>::create(startAt, num)};
}
Schedule Schedule::everyWeek(QDateTime startAt, int num, QList<int> daysOfWeek)
{
    return {QSharedPointer<EveryWeekSchedule>::create(startAt, num, std::move(daysOfWeek))};
}
Schedule Schedule::everyMonth(QDateTime startAt, int num, QList<int> daysOfMonth)
{
    return {QSharedPointer<EveryMonthSchedule>::create(startAt, num, std::move(daysOfMonth))};
}

void Schedule::start()
{
    _impl->start();
}
void Schedule::stop()
{
    _impl->stop();
}
void Schedule::triggerEvent()
{
    emit event();
}
QString Schedule::toString() const
{
    return _impl->toString();
}
QJsonObject Schedule::toJson() const
{
    return _impl->toJson();
}
Schedule Schedule::fromJson(QJsonObject obj)
{
    AbstractSchedule* schedule;
    if (schedule = WhenDirChangedSchedule::fromJson(obj))
        return QSharedPointer<AbstractSchedule>(schedule);
    if (schedule = EveryMinuteSchedule::fromJson(obj))
        return QSharedPointer<AbstractSchedule>(schedule);
    if (schedule = EveryHourSchedule::fromJson(obj))
        return QSharedPointer<AbstractSchedule>(schedule);
    if (schedule = EveryDaySchedule::fromJson(obj))
        return QSharedPointer<AbstractSchedule>(schedule);
    if (schedule = EveryWeekSchedule::fromJson(obj))
        return QSharedPointer<AbstractSchedule>(schedule);
    if (schedule = EveryMonthSchedule::fromJson(obj))
        return QSharedPointer<AbstractSchedule>(schedule);
    return QSharedPointer<AbstractSchedule>(schedule);
}

Schedule::Schedule() :
    _impl(QSharedPointer<EveryMinuteSchedule>::create(QDateTime::currentDateTime(), 1))
{
    connect(_impl.get(), &AbstractSchedule::event, this, &Schedule::triggerEvent);
}

Schedule::Schedule(const Schedule& other) :
    _impl(other._impl)
{
    connect(_impl.get(), &AbstractSchedule::event, this, &Schedule::triggerEvent);
}

Schedule::Schedule(QSharedPointer<AbstractSchedule> impl) :
    _impl(impl)
{
    connect(_impl.get(), &AbstractSchedule::event, this, &Schedule::triggerEvent);
}

Schedule& Schedule::operator=(const Schedule& other)
{
    if (this == &other)
        return *this;

    _impl = other._impl;
    return *this;
}