#pragma once

#include "Schedules.hpp"
#include <QObject>
#include <QTime>
#include <QDate>
#include <QDateTime>
#include <QList>
#include <QSharedPointer>

class Schedule final : public AbstractSchedule
{
    Q_OBJECT

public:
    Schedule();
    Schedule(const Schedule&);
    Schedule& operator=(const Schedule&);

    static Schedule manually();
    static Schedule immediately();
    static Schedule once(
        QDateTime at = QDateTime::currentDateTime());
    static Schedule whenDirChanged(
        QString path,
        int waitInMsecs = 0);
    static Schedule everyMinute(
        QDateTime startAt = QDateTime::currentDateTime(),
        int num = 1);
    static Schedule everyHour(
        QDateTime startAt = QDateTime::currentDateTime(),
        int num = 1);
    static Schedule everyDay(
        QDateTime startAt = QDateTime::currentDateTime(),
        int num = 1);
    static Schedule everyWeek(
        QDateTime startAt = QDateTime::currentDateTime(),
        int num = 1,
        QList<int> daysOfWeek = {QDate::currentDate().dayOfWeek()});
    static Schedule everyMonth(
        QDateTime startAt = QDateTime::currentDateTime(),
        int num = 1,
        QList<int> daysOfMonth = {QDate::currentDate().day()});

    QString toString() const override;
    QJsonObject toJson() const override;
    static Schedule fromJson(QJsonObject);

public slots:
    void start() override;
    void stop() override;
    void triggerEvent() override;

private:
    Schedule(QSharedPointer<AbstractSchedule>);
    QSharedPointer<AbstractSchedule> _impl;
};