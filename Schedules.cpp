#include "Schedules.hpp"
#include <QJsonArray>
#include <QDebug>

namespace
{
constexpr qint64 MSECCS_IN_SEC = 1000;
constexpr qint64 SECS_IN_MIN = 60; 
constexpr qint64 MINS_IN_HOUR = 60; 
constexpr qint64 HOURS_IN_DAY = 24; 
constexpr qint64 DAYS_IN_WEEK = 7; 
constexpr qint64 MONTHS_IN_YEAR = 12; 

constexpr qint64 SECS_IN_HOUR = SECS_IN_MIN * MINS_IN_HOUR; 
constexpr qint64 SECS_IN_DAY = SECS_IN_HOUR * HOURS_IN_DAY; 
constexpr qint64 SECS_IN_WEEK = SECS_IN_DAY * DAYS_IN_WEEK; 
constexpr qint64 MSECS_IN_DAY = SECS_IN_DAY * MSECCS_IN_SEC;

void verifyIntGreaterThanZero(int &num)
{
    if (num < 1)
    {
        qWarning() << "Num should be > 0";
        num = 1;
    }
}
QString intsToString(const QList<int>& list)
{
    QStringList sl{};
    for (int num : list)
        sl.append(QString::number(num));
    return sl.join(',');
}

QString daysOfWeekToString(const QList<int>& daysOfWeek)
{
    QStringList sl{};
    for (int day : daysOfWeek)
        sl.append(QLocale::system().dayName(day, QLocale::ShortFormat));
    return sl.join(',');
}

QJsonArray listToJsonArray(const QList<int>& list)
{
    QJsonArray arr{};
    for (int num : list)
        arr.append(num);
    return arr;
}

QList<int> jsonArrayToList(QJsonArray arr)
{
    QList<int> list{};
    for (auto num : arr)
        list.append(num.toInt());
    return list;
}

qint64 weeksTo(QDateTime from, QDateTime to)
{
    const QDateTime fromStartDay = from.addDays(-from.date().dayOfWeek());
    const QDateTime toStartDay = to.addDays(-to.date().dayOfWeek());
    return fromStartDay.daysTo(toStartDay) / DAYS_IN_WEEK;
}

qint64 monthsTo(QDateTime from, QDateTime to)
{
    const int yearsTo = to.date().year() - from.date().year();
    return yearsTo * MONTHS_IN_YEAR + to.date().month() - from.date().month();
}

} // namespace

WhenDirChangedSchedule::WhenDirChangedSchedule(QString path, int waitInMsecs) :
    _timer(),
    _watcher(),
    _path(path),
    _waitInMsecs(waitInMsecs)
{
    connect(&_watcher, &QFileSystemWatcher::fileChanged, [this](){ _timer.start(_waitInMsecs); });
    connect(&_watcher, &QFileSystemWatcher::directoryChanged, [this](){ _timer.start(_waitInMsecs); });
    connect(&_timer, &QTimer::timeout, this, &WhenDirChangedSchedule::timeout);
}
WhenDirChangedSchedule::~WhenDirChangedSchedule()
{
    if (_timer.isActive()) _timer.stop();
}
QString WhenDirChangedSchedule::toString() const
{
    return "🗀 : " + _path + " ⧖: " + QString::number(_waitInMsecs) + "ms";
}
QJsonObject WhenDirChangedSchedule::toJson() const
{
    QJsonObject obj;
    obj["type"] = "when dir changed";
    obj["path"] = _path;
    obj["wait in ms"] = _waitInMsecs;
    return obj;
}
AbstractSchedule* WhenDirChangedSchedule::fromJson(QJsonObject obj)
{
    if (obj["type"] != "when dir changed")
        return nullptr;
    return new WhenDirChangedSchedule(
        obj["path"].toString(),
        obj["wait in ms"].toInt());
}
void WhenDirChangedSchedule::start()
{
    _watcher.addPath(_path);
}
void WhenDirChangedSchedule::stop()
{
    _watcher.removePath(_path);
    _timer.stop();
}
void WhenDirChangedSchedule::triggerEvent()
{
    emit event();
}
void WhenDirChangedSchedule::timeout()
{
    _timer.stop();
    triggerEvent();
}

DateTimeSchedule::DateTimeSchedule(QDateTime startAt) :
    _startAt(startAt),
    _timer(),
    _isSubtimeout()
{
    _timer.setTimerType(Qt::TimerType::PreciseTimer);
    connect(&_timer, &QTimer::timeout, this, &DateTimeSchedule::timeout);
}
DateTimeSchedule::~DateTimeSchedule()
{
    if (_timer.isActive()) _timer.stop();
}
void DateTimeSchedule::start()
{
    const auto current = QDateTime::currentDateTime();
    const auto nearest = _startAt >= current ? _startAt : nearestTrigger();
    qint64 msecsToNearest = current.msecsTo(nearest);

    // Remark: start timer max to one day, because timer input value is int
    // Issue: https://bugreports.qt.io/browse/QTBUG-67383
    msecsToNearest = msecsToNearest > MSECS_IN_DAY ? MSECS_IN_DAY : msecsToNearest;
    _isSubtimeout = msecsToNearest > MSECS_IN_DAY;

    _timer.start(msecsToNearest);
}
void DateTimeSchedule::stop()
{
    _timer.stop();
}
void DateTimeSchedule::triggerEvent()
{
    emit event();
}
void DateTimeSchedule::timeout()
{
    const auto current = QDateTime::currentDateTime();
    const auto nearest = _startAt >= current ? _startAt : nearestTrigger();
    qint64 msecsToNearest = current.msecsTo(nearest);

    if (not _isSubtimeout)
        triggerEvent();

    msecsToNearest = msecsToNearest > MSECS_IN_DAY ? MSECS_IN_DAY : msecsToNearest;
    _isSubtimeout = msecsToNearest > MSECS_IN_DAY;

    _timer.start(msecsToNearest);
}
QString DateTimeSchedule::toString() const
{
    return "⏲ : " + _startAt.toString("dd.MM.yyyy hh:mm:ss");
}
QJsonObject DateTimeSchedule::toJson() const
{
    QJsonObject obj;
    obj["start at"] = _startAt.toString("dd.MM.yyyy hh:mm:ss");
    return obj;
}
QDateTime DateTimeSchedule::startAtFromJson(QJsonObject obj)
{
    return QDateTime::fromString(obj["start at"].toString(), "dd.MM.yyyy hh:mm:ss");
}

EveryMinuteSchedule::EveryMinuteSchedule(QDateTime startAt, int num) :
    DateTimeSchedule(startAt),
    _num(num)
{
    verifyIntGreaterThanZero(_num);
}
QString EveryMinuteSchedule::toString() const
{
    return "🗘 : " + QString::number(_num) + "min " + DateTimeSchedule::toString();
}
QJsonObject EveryMinuteSchedule::toJson() const
{
    QJsonObject obj = DateTimeSchedule::toJson();
    obj["type"] = "every min";
    obj["num"] = _num;
    return obj;
}
AbstractSchedule* EveryMinuteSchedule::fromJson(QJsonObject obj)
{
    if (obj["type"] != "every min")
        return nullptr;
    return new EveryMinuteSchedule(
        startAtFromJson(obj),
        obj["num"].toInt());
}
QDateTime EveryMinuteSchedule::nearestTrigger() const
{
    const auto current = QDateTime::currentDateTime();
    const qint64 minsToCurrent = _startAt.secsTo(current) / SECS_IN_MIN;
    const qint64 timesToNearest = (minsToCurrent / _num) + 1;

    return _startAt.addSecs(timesToNearest * _num * SECS_IN_MIN);
}

EveryHourSchedule::EveryHourSchedule(QDateTime startAt, int num) :
    DateTimeSchedule(startAt),
    _num(num)
{
    verifyIntGreaterThanZero(_num);
}
QString EveryHourSchedule::toString() const
{
    return "🗘 : " + QString::number(_num) + "h " + DateTimeSchedule::toString();
}
QJsonObject EveryHourSchedule::toJson() const
{
    QJsonObject obj = DateTimeSchedule::toJson();
    obj["type"] = "every hour";
    obj["num"] = _num;
    return obj;
}
AbstractSchedule* EveryHourSchedule::fromJson(QJsonObject obj)
{
    if (obj["type"] != "every hour")
        return nullptr;
    return new EveryHourSchedule(
        startAtFromJson(obj),
        obj["num"].toInt());
}
QDateTime EveryHourSchedule::nearestTrigger() const
{
    const auto current = QDateTime::currentDateTime();
    const qint64 hoursToCurrent = _startAt.secsTo(current) / SECS_IN_HOUR;
    const qint64 timesToNearest = (hoursToCurrent / _num) + 1;

    return _startAt.addSecs(timesToNearest * _num * SECS_IN_HOUR);
}

EveryDaySchedule::EveryDaySchedule(QDateTime startAt, int num) :
    DateTimeSchedule(startAt),
    _num(num)
{
    verifyIntGreaterThanZero(_num);
}
QString EveryDaySchedule::toString() const
{
    return "🗘 : " + QString::number(_num) + "day " + DateTimeSchedule::toString();
}
QJsonObject EveryDaySchedule::toJson() const
{
    QJsonObject obj = DateTimeSchedule::toJson();
    obj["type"] = "every day";
    obj["num"] = _num;
    return obj;
}
AbstractSchedule* EveryDaySchedule::fromJson(QJsonObject obj)
{
    if (obj["type"] != "every day")
        return nullptr;
    return new EveryDaySchedule(
        startAtFromJson(obj),
        obj["num"].toInt());
}
QDateTime EveryDaySchedule::nearestTrigger() const
{
    const auto current = QDateTime::currentDateTime();
    // Remark: daysTo doesn't consider time
    const qint64 daysToCurrent = _startAt.secsTo(current) / SECS_IN_DAY;
    const qint64 timesToNearest = (daysToCurrent / _num) + 1;

    return _startAt.addDays(timesToNearest * _num);
}

EveryWeekSchedule::EveryWeekSchedule(QDateTime startAt, int num, QList<int> daysOfWeek) :
    DateTimeSchedule(startAt),
    _num(num),
    _daysOfWeek(std::move(daysOfWeek))
{
    verifyIntGreaterThanZero(_num);
}
QString EveryWeekSchedule::toString() const
{
    return "🗘 : " + QString::number(_num) + "week days: " + daysOfWeekToString(_daysOfWeek) + " " + DateTimeSchedule::toString();
}
QJsonObject EveryWeekSchedule::toJson() const
{
    QJsonObject obj = DateTimeSchedule::toJson();
    obj["type"] = "every week";
    obj["num"] = _num;
    obj["days of week"] = listToJsonArray(_daysOfWeek);
    return obj;
}
AbstractSchedule* EveryWeekSchedule::fromJson(QJsonObject obj)
{
    if (obj["type"] != "every week")
        return nullptr;
    return new EveryWeekSchedule(
        startAtFromJson(obj),
        obj["num"].toInt(),
        jsonArrayToList(obj["days of week"].toArray()));
}
QDateTime EveryWeekSchedule::nearestTrigger() const
{
    const auto current = QDateTime::currentDateTime();
    const qint64 weeksToCurrent = weeksTo(_startAt, current);
    const qint64 timesToCurrent = weeksToCurrent / _num;
    const auto currentWeek = _startAt.addDays(timesToCurrent * _num * DAYS_IN_WEEK);
    const auto currentWeekStart = currentWeek.addDays(-currentWeek.date().dayOfWeek());

    for (int dayOfWeek : _daysOfWeek)
    {
        const auto candidate = currentWeekStart.addDays(dayOfWeek);
        if (candidate > current)
            return candidate;
    }
    return currentWeekStart.addDays(_daysOfWeek.first()).addDays(_num * DAYS_IN_WEEK);
}

EveryMonthSchedule::EveryMonthSchedule(QDateTime startAt, int num, QList<int> daysOfMonth) :
    DateTimeSchedule(startAt),
    _num(num),
    _daysOfMonth(std::move(daysOfMonth))
{
    verifyIntGreaterThanZero(_num);
}
QString EveryMonthSchedule::toString() const
{
    return "🗘 : " + QString::number(_num) + "month days: " + intsToString(_daysOfMonth) + " " + DateTimeSchedule::toString();
}
QJsonObject EveryMonthSchedule::toJson() const
{
    QJsonObject obj = DateTimeSchedule::toJson();
    obj["type"] = "every month";
    obj["num"] = _num;
    obj["days of month"] = listToJsonArray(_daysOfMonth);
    return obj;
}
AbstractSchedule* EveryMonthSchedule::fromJson(QJsonObject obj)
{
    if (obj["type"] != "every month")
        return nullptr;
    return new EveryMonthSchedule(
        startAtFromJson(obj),
        obj["num"].toInt(),
        jsonArrayToList(obj["days of month"].toArray()));
}
QDateTime EveryMonthSchedule::nearestTrigger() const
{
    const auto current = QDateTime::currentDateTime();
    const qint64 monthsToCurrent = monthsTo(_startAt, current);
    const qint64 timesToCurrent = monthsToCurrent / _num;
    const auto currentMonth = _startAt.addMonths(timesToCurrent);
    const auto currentMonthStart = currentMonth.addDays(-currentMonth.date().day());

    for (int dayOfMonth : _daysOfMonth)
    {
        if (dayOfMonth > currentMonth.date().daysInMonth())
            dayOfMonth = currentMonth.date().daysInMonth();
        const auto candidate = currentMonthStart.addDays(dayOfMonth);
        if (candidate > current)
            return candidate;
    }

    const auto nearestMonth = currentMonth.addMonths(_num);
    const auto nearestMonthStart = nearestMonth.addDays(-nearestMonth.date().day());
    if (_daysOfMonth.first() > nearestMonth.date().daysInMonth())
        return nearestMonthStart.addDays(nearestMonth.date().daysInMonth());
    return nearestMonthStart.addDays(_daysOfMonth.first());
}