#pragma once

#include <QObject>
#include <QString>
#include <QJsonObject>
#include <QTime>
#include <QDate>
#include <QDateTime>
#include <QList>
#include <QDir>
#include <QTimer>
#include <QFileSystemWatcher>

class AbstractSchedule : public QObject
{
    Q_OBJECT

public:
    virtual QString toString() const = 0;
    virtual QJsonObject toJson() const = 0;

public slots:
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void triggerEvent() = 0;

signals:
    void event();
};

class WhenDirChangedSchedule : public AbstractSchedule
{
    Q_OBJECT

public:
    WhenDirChangedSchedule(QString path, int waitInMsecs);
    ~WhenDirChangedSchedule();

    QString toString() const override;
    QJsonObject toJson() const override;
    static AbstractSchedule* fromJson(QJsonObject);

public slots:
    void start() override;
    void stop() override;
    void triggerEvent() override;

private slots:
    void timeout();

private:
    QTimer _timer;
    QFileSystemWatcher _watcher;
    QString _path;
    int _waitInMsecs;
};

class DateTimeSchedule : public AbstractSchedule
{
    Q_OBJECT

public:
    DateTimeSchedule(QDateTime startAt);
    ~DateTimeSchedule();

    QString toString() const override;
    QJsonObject toJson() const override;

public slots:
    void start() override;
    void stop() override;
    void triggerEvent() override;

private slots:
    void timeout();

protected:
    virtual QDateTime nearestTrigger() const = 0;
    static QDateTime startAtFromJson(QJsonObject);

    QDateTime _startAt;

private:
    QTimer _timer;
    bool _isSubtimeout;
};

class EveryMinuteSchedule final : public DateTimeSchedule
{
    Q_OBJECT

public:
    EveryMinuteSchedule(QDateTime startAt, int num);

    QString toString() const override;
    QJsonObject toJson() const override;
    static AbstractSchedule* fromJson(QJsonObject);

protected:
    QDateTime nearestTrigger() const override;

private:
    int _num;
};

class EveryHourSchedule final : public DateTimeSchedule
{
    Q_OBJECT

public:
    EveryHourSchedule(QDateTime startAt, int num);

    QString toString() const override;
    QJsonObject toJson() const override;
    static AbstractSchedule* fromJson(QJsonObject);

protected:
    QDateTime nearestTrigger() const override;

private:
    int _num;
};

class EveryDaySchedule final : public DateTimeSchedule
{
    Q_OBJECT

public:
    EveryDaySchedule(QDateTime startAt, int num);

    QString toString() const override;
    QJsonObject toJson() const override;
    static AbstractSchedule* fromJson(QJsonObject);

protected:
    QDateTime nearestTrigger() const override;

private:
    int _num;
};

class EveryWeekSchedule final : public DateTimeSchedule
{
    Q_OBJECT

public:
    EveryWeekSchedule(QDateTime startAt, int num, QList<int> daysOfWeek);

    QString toString() const override;
    QJsonObject toJson() const override;
    static AbstractSchedule* fromJson(QJsonObject);

protected:
    QDateTime nearestTrigger() const override;

private:
    int _num;
    QList<int> _daysOfWeek;
};

class EveryMonthSchedule final : public DateTimeSchedule
{
    Q_OBJECT

public:
    EveryMonthSchedule(QDateTime startAt, int num, QList<int> daysOfMonth);

    QString toString() const override;
    QJsonObject toJson() const override;
    static AbstractSchedule* fromJson(QJsonObject);

protected:
    QDateTime nearestTrigger() const override;

private:
    int _num;
    QList<int> _daysOfMonth;
};