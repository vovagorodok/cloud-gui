#include "SysTrayMenuPositionCalc.hpp"
#include <QGuiApplication>
#include <QWindow>
#include <QScreen>
#include <QDebug>

#if defined(Q_OS_WIN)
#include <comdef.h>
#include <shlguid.h>
#include <shlobj.h>
#include <string>
#include <winbase.h>
#include <windows.h>
#include <winerror.h>
#endif

namespace
{

#if defined(Q_OS_WIN)
bool registryKeyExists(HKEY hRootKey, const QString &subKey)
{
    HKEY hKey;

    REGSAM sam = KEY_READ | KEY_WOW64_64KEY;
    LONG result = RegOpenKeyEx(hRootKey, reinterpret_cast<LPCWSTR>(subKey.utf16()), 0, sam, &hKey);

    RegCloseKey(hKey);
    return result != ERROR_FILE_NOT_FOUND;
}

QVariant registryGetKeyValue(HKEY hRootKey, const QString &subKey, const QString &valueName)
{
    QVariant value;

    HKEY hKey;

    REGSAM sam = KEY_READ | KEY_WOW64_64KEY;
    LONG result = RegOpenKeyEx(hRootKey, reinterpret_cast<LPCWSTR>(subKey.utf16()), 0, sam, &hKey);
    ASSERT(result == ERROR_SUCCESS || result == ERROR_FILE_NOT_FOUND);
    if (result != ERROR_SUCCESS)
        return value;

    DWORD type = 0, sizeInBytes = 0;
    result = RegQueryValueEx(hKey, reinterpret_cast<LPCWSTR>(valueName.utf16()), 0, &type, nullptr, &sizeInBytes);
    ASSERT(result == ERROR_SUCCESS || result == ERROR_FILE_NOT_FOUND);
    if (result == ERROR_SUCCESS) {
        switch (type) {
        case REG_DWORD:
            DWORD dword;
            Q_ASSERT(sizeInBytes == sizeof(dword));
            if (RegQueryValueEx(hKey, reinterpret_cast<LPCWSTR>(valueName.utf16()), 0, &type, reinterpret_cast<LPBYTE>(&dword), &sizeInBytes) == ERROR_SUCCESS) {
                value = int(dword);
            }
            break;
        case REG_EXPAND_SZ:
        case REG_SZ: {
            QString string;
            string.resize(sizeInBytes / sizeof(QChar));
            result = RegQueryValueEx(hKey, reinterpret_cast<LPCWSTR>(valueName.utf16()), 0, &type, reinterpret_cast<LPBYTE>(string.data()), &sizeInBytes);

            if (result == ERROR_SUCCESS) {
                int newCharSize = sizeInBytes / sizeof(QChar);
                // From the doc:
                // If the data has the REG_SZ, REG_MULTI_SZ or REG_EXPAND_SZ type, the string may not have been stored with
                // the proper terminating null characters. Therefore, even if the function returns ERROR_SUCCESS,
                // the application should ensure that the string is properly terminated before using it; otherwise, it may overwrite a buffer.
                if (string.at(newCharSize - 1) == QLatin1Char('\0'))
                    string.resize(newCharSize - 1);
                value = string;
            }
            break;
        }
        case REG_BINARY: {
            QByteArray buffer;
            buffer.resize(sizeInBytes);
            result = RegQueryValueEx(hKey, reinterpret_cast<LPCWSTR>(valueName.utf16()), 0, &type, reinterpret_cast<LPBYTE>(buffer.data()), &sizeInBytes);
            if (result == ERROR_SUCCESS) {
                value = buffer.at(12);
            }
            break;
        }
        default:
            Q_UNREACHABLE();
        }
    }
    ASSERT(result == ERROR_SUCCESS || result == ERROR_FILE_NOT_FOUND);

    RegCloseKey(hKey);
    return value;
}

QRect getTaskbarDimensions()
{
    APPBARDATA barData;
    barData.cbSize = sizeof(APPBARDATA);

    BOOL fResult = (BOOL)SHAppBarMessage(ABM_GETTASKBARPOS, &barData);
    if (!fResult) {
        return QRect();
    }

    RECT barRect = barData.rc;
    return QRect(barRect.left, barRect.top, (barRect.right - barRect.left), (barRect.bottom - barRect.top));
}
#endif

QScreen *currentScreen()
{
    const auto screens = QGuiApplication::screens();
    const auto cursorPos = QCursor::pos();

    for (const auto screen : screens) {
        if (screen->geometry().contains(cursorPos)) {
            return screen;
        }
    }

    return QGuiApplication::primaryScreen();
}

QRect currentScreenRect()
{
    const auto screen = currentScreen();
    Q_ASSERT(screen);
    return screen->geometry();
}

} // namespace

SysTrayMenuPositionCalc::SysTrayMenuPositionCalc(const QSystemTrayIcon *trayIcon) :
    trayIcon(trayIcon) {}

void SysTrayMenuPositionCalc::positionWindow(QWidget *window) const
{
    // window->setScreen(currentScreen());
    window->windowHandle()->setScreen(currentScreen());
    const auto position = computeWindowPosition(window->width(), window->height());
    window->move(position);
    // window->setPosition(position);
}

QPoint SysTrayMenuPositionCalc::calcTrayIconCenter() const
{
    // QSystemTrayIcon::geometry() is broken for ages on most Linux DEs (invalid geometry returned)
    // thus we can use this only for Windows and macOS
#if defined(Q_OS_WIN) || defined(Q_OS_MACOS)
    auto trayIconCenter = trayIcon->geometry().center();
    return trayIconCenter;
#else
    // On Linux, fall back to mouse position (assuming tray icon is activated by mouse click)
    return QCursor::pos(currentScreen());
#endif
}

SysTrayMenuPositionCalc::TaskBarPosition SysTrayMenuPositionCalc::taskbarOrientation() const
{
// macOS: Always on top
#if defined(Q_OS_MACOS)
    return TaskBarPosition::Top;
// Windows: Check registry for actual taskbar orientation
#elif defined(Q_OS_WIN)
    auto taskbarPositionSubkey = QStringLiteral("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StuckRects3");
    if (!registryKeyExists(HKEY_CURRENT_USER, taskbarPositionSubkey)) {
        // Windows 7
        taskbarPositionSubkey = QStringLiteral("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\StuckRects2");
    }
    if (!registryKeyExists(HKEY_CURRENT_USER, taskbarPositionSubkey)) {
        return TaskBarPosition::Bottom;
    }
    auto taskbarPosition = registryGetKeyValue(HKEY_CURRENT_USER, taskbarPositionSubkey, "Settings");
    switch (taskbarPosition.toInt()) {
    // Mapping windows binary value (0 = left, 1 = top, 2 = right, 3 = bottom) to qml logic (0 = bottom, 1 = left...)
    case 0:
        return TaskBarPosition::Left;
    case 1:
        return TaskBarPosition::Top;
    case 2:
        return TaskBarPosition::Right;
    case 3:
        return TaskBarPosition::Bottom;
    default:
        return TaskBarPosition::Bottom;
    }
// Probably Linux
#else
    const auto screenRect = currentScreenRect();
    const auto trayIconCenter = calcTrayIconCenter();

    const auto distBottom = screenRect.bottom() - trayIconCenter.y();
    const auto distRight = screenRect.right() - trayIconCenter.x();
    const auto distLeft = trayIconCenter.x() - screenRect.left();
    const auto distTop = trayIconCenter.y() - screenRect.top();

    const auto minDist = std::min({distRight, distTop, distBottom});

    if (minDist == distBottom) {
        return TaskBarPosition::Bottom;
    } else if (minDist == distLeft) {
        return TaskBarPosition::Left;
    } else if (minDist == distTop) {
        return TaskBarPosition::Top;
    } else {
        return TaskBarPosition::Right;
    }
#endif
}

// TODO: Get real taskbar dimensions Linux as well
QRect SysTrayMenuPositionCalc::taskbarGeometry() const
{
#if defined(Q_OS_WIN)
    QRect tbRect = getTaskbarDimensions();
    //QML side expects effective pixels, convert taskbar dimensions if necessary
    auto pixelRatio = currentScreen()->devicePixelRatio();
    if (pixelRatio != 1) {
        tbRect.setHeight(tbRect.height() / pixelRatio);
        tbRect.setWidth(tbRect.width() / pixelRatio);
    }
    return tbRect;
#elif defined(Q_OS_MACOS)
    // Finder bar is always 22px height on macOS (when treating as effective pixels)
    auto screenWidth = currentScreenRect().width();
    return {0, 0, screenWidth, 22};
#else
    if (taskbarOrientation() == TaskBarPosition::Bottom || taskbarOrientation() == TaskBarPosition::Top) {
        auto screenWidth = currentScreenRect().width();
        return {0, 0, screenWidth, 32};
    } else {
        auto screenHeight = currentScreenRect().height();
        return {0, 0, 32, screenHeight};
    }
#endif
}

QPoint SysTrayMenuPositionCalc::computeWindowReferencePoint() const
{
    constexpr auto spacing = 4;
    const auto trayIconCenter = calcTrayIconCenter();
    const auto taskbarRect = taskbarGeometry();
    const auto taskbarScreenEdge = taskbarOrientation();
    const auto screenRect = currentScreenRect();

    // qDebug() << "screenRect:" << screenRect;
    // qDebug() << "taskbarRect:" << taskbarRect;
    // qDebug() << "taskbarScreenEdge:" << taskbarScreenEdge;
    // qDebug() << "trayIconCenter:" << trayIconCenter;

    switch(taskbarScreenEdge) {
    case TaskBarPosition::Bottom:
        return {
            trayIconCenter.x(),
            screenRect.bottom() - taskbarRect.height() - spacing
        };
    case TaskBarPosition::Left:
        return {
            screenRect.left() + taskbarRect.width() + spacing,
            trayIconCenter.y()
        };
    case TaskBarPosition::Top:
        return {
            trayIconCenter.x(),
            screenRect.top() + taskbarRect.height() + spacing
        };
    case TaskBarPosition::Right:
        return {
            screenRect.right() - taskbarRect.width() - spacing,
            trayIconCenter.y()
        };
    }
    Q_UNREACHABLE();
}

QPoint SysTrayMenuPositionCalc::computeWindowPosition(int width, int height) const
{
    const auto referencePoint = computeWindowReferencePoint();

    const auto taskbarScreenEdge = taskbarOrientation();
    const auto screenRect = currentScreenRect();

    const auto topLeft = [=]() {
        switch(taskbarScreenEdge) {
        case TaskBarPosition::Bottom:
            return referencePoint - QPoint(width / 2, height);
        case TaskBarPosition::Left:
            return referencePoint;
        case TaskBarPosition::Top:
            return referencePoint - QPoint(width / 2, 0);
        case TaskBarPosition::Right:
            return referencePoint - QPoint(width, 0);
        }
        Q_UNREACHABLE();
    }();
    const auto bottomRight = topLeft + QPoint(width, height);
    const auto windowRect = [=]() {
        const auto rect = QRect(topLeft, bottomRight);
        auto offset = QPoint();

        if (rect.left() < screenRect.left()) {
            offset.setX(screenRect.left() - rect.left() + 4);
        } else if (rect.right() > screenRect.right()) {
            offset.setX(screenRect.right() - rect.right() - 4);
        }

        if (rect.top() < screenRect.top()) {
            offset.setY(screenRect.top() - rect.top() + 4);
        } else if (rect.bottom() > screenRect.bottom()) {
            offset.setY(screenRect.bottom() - rect.bottom() - 4);
        }

        return rect.translated(offset);
    }();

    // qDebug() << "taskbarScreenEdge:" << taskbarScreenEdge;
    // qDebug() << "screenRect:" << screenRect;
    // qDebug() << "windowRect (reference)" << QRect(topLeft, bottomRight);
    // qDebug() << "windowRect (adjusted)" << windowRect;

    return windowRect.topLeft();
}