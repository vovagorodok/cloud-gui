#pragma once

#include <QWidget>
#include <QSystemTrayIcon>

// Solution copied from:
// https://github.com/nextcloud/desktop/blob/master/src/gui/systray.cpp
// until/if will be found better

class SysTrayMenuPositionCalc
{
public:
    SysTrayMenuPositionCalc(const QSystemTrayIcon *trayIcon);
    void positionWindow(QWidget *window) const;
private:
    enum TaskBarPosition { Bottom, Left, Top, Right };

    QPoint calcTrayIconCenter() const;
    TaskBarPosition taskbarOrientation() const;
    QRect taskbarGeometry() const;
    QPoint computeWindowReferencePoint() const;
    QPoint computeWindowPosition(int width, int height) const;

    const QSystemTrayIcon *trayIcon;
};