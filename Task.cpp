#include "Task.hpp"
#include "Remote.hpp"
#include <QDebug>

Task::Task(Remote& remote, const QJsonObject& config) :
    _remote(remote),
    _config{stringToEnum<Type>(config["type"].toString()),
            stringToEnum<Direction>(config["direction"].toString()),
            Schedule::fromJson(config["schedule"].toObject()),
            config["localPath"].toString(),
            config["remotePath"].toString(),
            config["additionalParams"].toObject()}
{
    connect(&_config.schedule, &Schedule::event, this, &Task::start);
    enable();
}

Task::Task(Remote& remote, const Config& config) :
    _remote(remote),
    _config(config)
{
    connect(&_config.schedule, &Schedule::event, this, &Task::start);
    enable();
}

void Task::deleteTask()
{
    delete this;
}

const Task::Config& Task::config()
{
    return _config;
}

void Task::start()
{
    qDebug() << "Run task: " << QTime::currentTime();
}

void Task::stop()
{
}

void Task::enable()
{
    _config.schedule.start();
}

void Task::disable()
{
    _config.schedule.stop();
}

QJsonObject Task::saveConfig()
{
    QJsonObject save;
    save["type"] = enumToString(_config.type);
    save["direction"] = enumToString(_config.direction);
    save["schedule"] = _config.schedule.toJson();
    save["localPath"] = _config.localPath;
    save["remotePath"] = _config.remotePath;
    save["additionalParams"] = _config.additionalParams;
    return save;
}