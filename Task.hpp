#pragma once

#include "RcloneEngine.hpp"
#include "Schedule.hpp"
#include <QObject>
#include <QJsonObject>

class Remote;

template <typename EnumT>
EnumT stringToEnum(QString str)
{
    return qvariant_cast<EnumT>(str);
}
template <typename EnumT>
QString enumToString(EnumT en)
{
    return QVariant::fromValue(en).toString();
}

class Task final : public QObject
{
    Q_OBJECT

public:
    enum class Type
    {
        Mount,
        Sync,
        Copy,
        Move
    };
    Q_ENUM(Type)

    enum class Direction
    {
        None,
        ToRemote,
        ToLocal,
        Both
    };
    Q_ENUM(Direction)
    
    struct Config
    {
        Type type;
        Direction direction;
        Schedule schedule;
        QString localPath;
        QString remotePath;
        QJsonObject additionalParams;
    };

    explicit Task(Remote& remote, const QJsonObject& config);
    explicit Task(Remote& remote, const Config& config);
    void deleteTask();
    const Config& config();

public slots:
    void start();
    void stop();
    void enable();
    void disable();

    QJsonObject saveConfig();

private:
    Remote& _remote;
    Config _config;
};