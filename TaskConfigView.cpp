#include "TaskConfigView.hpp"
#include "TaskItemView.hpp"
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include <QFile>
#include <QLineEdit>
#include <QLabel>
#include <QFormLayout>
#include <QComboBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QSpinBox>
#include <QRegExpValidator>
#include <QDir>
#include <QMessageBox>
#include <QDebug>
#include <algorithm>

TaskConfigView::TaskConfigView(QWidget *parent) :
    QWidget(parent),
    _configs(Engine::getTaskTemplates()),
    _taskParams()
{
    ui.setupUi(this);

    for (const auto& optionType : {"Simple", "Normal", "Advanced"})
        ui.optionsTypeComboBox->addItem(optionType);
    ui.directionComboBox->addItem("From local to remote cloud");
    ui.directionComboBox->addItem("From remote cloud to local");

    connect(ui.optionsTypeComboBox, qOverload<int>(&QComboBox::currentIndexChanged),
            this, &TaskConfigView::cloudTypeChanged);
    connect(ui.addPushButton, &QPushButton::clicked,
            this, &TaskConfigView::createConfig);
    connect(ui.cancelPushButton, &QPushButton::clicked,
            [this](){ emit canceled(); });

    initConfig();
}

void TaskConfigView::create(Remote& remote, Task::Type type)
{
    ui.directionLabel->setVisible(type != Task::Type::Mount);
    ui.directionComboBox->setVisible(type != Task::Type::Mount);
    _type = type;
    _currentRemote = &remote;
    initConfig();
}

void TaskConfigView::cloudTypeChanged(int index)
{
    initConfig();
}

void TaskConfigView::createConfig()
{
    Task::Config config{};
    config.type = _type;
    config.direction = currentDirection();
    config.schedule = Schedule::everyMinute();

    for (Option* opt : _taskParams)
    {
        const auto& value = opt->value();
        const auto name = value.first;
        if (name == "localPath")
            config.localPath = value.second.toString();
        else if (name == "remotePath")
            config.remotePath = value.second.toString();
        else
            config.additionalParams[name] = value.second;
    }

    QDir localDir(config.localPath);
    if (not localDir.exists())
    {
        auto reply = QMessageBox::question(this,
                                           "Local directory not exist",
                                           "Local directory not exist. Create it?",
                                           QMessageBox::Yes | QMessageBox::No);
        if (reply == QMessageBox::No) 
            return;
        localDir.mkpath(config.localPath);
    }

    auto remoteListDir = Engine::listDirectory(_currentRemote->name(), config.remotePath);
    if (remoteListDir.status == Rclone::Status::InternalError)
    {
        auto reply = QMessageBox::question(this,
                                           "Remote directory not exist",
                                           "Remote directory not exist. Create it?",
                                           QMessageBox::Yes | QMessageBox::No);
        if (reply == QMessageBox::No)
            return;
        if (Engine::makeDirectory(_currentRemote->name(), config.remotePath).status == Rclone::Status::InternalError)
        {
            emit error("Can't create remote directory");
            return;
        }
    }

    const bool isLocalDirEmpty = localDir.isEmpty();
    const bool isRemoteDirEmpty = remoteListDir.output.empty();

    if (_type == Task::Type::Mount and not isLocalDirEmpty)
    {
        emit error("Local directory is not empty");
        return;
    }

    QString syncDirectionStr = config.direction == Task::Direction::ToLocal ? "Local" : "Remote";
    if (_type == Task::Type::Sync and
        not isRemoteDirEmpty and not isLocalDirEmpty)
    {
        auto reply = QMessageBox::question(this,
                                           syncDirectionStr + " directory not empty",
                                           syncDirectionStr + " directory not empty. Data can be lost. Continue?",
                                           QMessageBox::Yes | QMessageBox::No);
        if (reply == QMessageBox::No)
            return;
    }

    // TODO: Check if SRC is empty and TARGET not. And ask to sync back

    Core::instance().createTask(*_currentRemote, config);
    emit created();
}

Task::Direction TaskConfigView::currentDirection()
{
    if (_type == Task::Type::Mount)
        return Task::Direction::None;
    if (ui.directionComboBox->currentIndex() == 0)
        return Task::Direction::ToRemote;
    return Task::Direction::ToLocal;
}

void TaskConfigView::initConfig()
{
    _taskParams.deleteAll();

    bool useSimpleOptions = ui.optionsTypeComboBox->currentText() == "Simple";

    QString typeName = _type == Task::Type::Mount ? "Mount" : "Sync";
    for (const auto config : _configs)
    {
        if (config["Name"] != typeName)
            continue;
        for (const auto option: config["Params"].toArray())
        {
            const auto config = option.toObject();
            bool isRequiredOption = config["Required"].toBool();
            bool isSimpleOption = isRequiredOption;
            if (useSimpleOptions and not isSimpleOption)
                continue;

            Option* opt = createTaskOption(config);
            _taskParams.add(opt);
            ui.optionsFormLayout->addRow(opt->label(), opt->widget());
        }
    }
}