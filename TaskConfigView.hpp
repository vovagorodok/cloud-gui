#pragma once

#include "ui_TaskConfigView.h"
#include "Core.hpp"
#include "RcloneEngine.hpp"
#include "Remote.hpp"
#include "AutoRemovebleObjectsList.hpp"
#include "Options.hpp"
#include <QWidget>
#include <QVector>
#include <QJsonArray>

class TaskConfigView : public QWidget
{
    Q_OBJECT

public:
    explicit TaskConfigView(QWidget *parent = nullptr);

signals:
    void canceled();
    void created();
    void error(QString msg);

public slots:
    void create(Remote&, Task::Type);

private slots:
    void cloudTypeChanged(int index);
    void createConfig();

private:
    Task::Direction currentDirection();
    void initConfig();

    Task::Type _type;
    Remote* _currentRemote;
    QVector<QJsonObject> _configs;
    OwnAutoRemovebleObjectsList<Option> _taskParams;
    Ui::TaskConfigView ui;
};