#include "TaskItemView.hpp"
#include <QJsonObject>
#include <QJsonArray>
#include <QString>
#include <QFile>
#include <QLineEdit>
#include <QLabel>
#include <QFormLayout>
#include <QComboBox>
#include <QVBoxLayout>
#include <QPushButton>
#include <QDebug>

TaskItemView::TaskItemView(Task& task, QWidget *parent) :
    ItemView(parent),
    _task(task)
{
    QGridLayout* layout = new QGridLayout();
    // layout->addWidget(new QLabel(enumToString(_task.config().type), this), 0, 0);
    // layout->addWidget(new QLabel("Last: ", this), 2, 0);
    // layout->addWidget(new QLabel("Remote: " + _task.config().remotePath, this), 0, 1);
    // layout->addWidget(new QLabel("Local: " + _task.config().localPath, this), 1, 1);
    // layout->addWidget(new QLabel("Schedule: " + _task.config().schedule.toString(), this), 2, 1);
    layout->addWidget(new QLabel(_task.config().schedule.toString(), this), 0, 0);
    ui.frame->setLayout(layout);

    connect(ui.yesButton, &QPushButton::clicked, this, &TaskItemView::deleteClicked);
    connect(ui.runButton, &QPushButton::clicked, this, &TaskItemView::runClicked);
}

void TaskItemView::showClicked()
{
}

void TaskItemView::deleteClicked()
{
    _task.deleteTask();
    delete this;
}

void TaskItemView::runClicked()
{
    _task.start();
}