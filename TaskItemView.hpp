#pragma once

#include "Task.hpp"
#include "ItemView.hpp"
#include "RcloneEngine.hpp"
#include <QFrame>

class TaskItemView : public ItemView
{
    Q_OBJECT

public:
    explicit TaskItemView(QWidget *parent = nullptr);
    explicit TaskItemView(Task& task, QWidget *parent = nullptr);

private slots:
    void showClicked();
    void deleteClicked();
    void runClicked();

private:
    Task& _task;
};