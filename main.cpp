#include <QString>
#include <QJsonObject>
#include <QJsonArray>
#include <QtGlobal>
#include <QApplication>
#include <QDebug>
#include <QMessageBox>
#include <QSystemTrayIcon>
#include "RcloneEngine.hpp"
#include "MainApp.hpp"
#include "MainView.hpp"
#include "RemoteConfigView.hpp"
#include "TaskConfigView.hpp"
#include "Core.hpp"

int main(int argc, char** argv) {
    QApplication app(argc, argv);

    if (not QSystemTrayIcon::isSystemTrayAvailable()) {
        QMessageBox::critical(0, QObject::tr("Systray"),
                              QObject::tr("I couldn't detect any system tray "
                                          "on this system."));
        return 1;
    }
    QApplication::setQuitOnLastWindowClosed(false);

    MainView *mainView = new MainView();
    RemoteConfigView *remoteConfigView = new RemoteConfigView();
    TaskConfigView *taskConfigView = new TaskConfigView();
    MainApp mainApp(mainView, remoteConfigView, taskConfigView);

    return app.exec();
}
